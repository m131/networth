import Process1 from '../../assets/image/saasModern/process-1.svg';
import Process2 from '../../assets/image/saasModern/process-2.svg';
import Process3 from '../../assets/image/saasModern/process-3.svg';

import FeatureIcon1 from '../../assets/image/saasModern/icon-1.png';
import FeatureIcon2 from '../../assets/image/saasModern/icon-2.png';
import FeatureIcon3 from '../../assets/image/saasModern/icon-3.png';
import FeatureIcon4 from '../../assets/image/saasModern/icon-4.png';
import FeatureIcon5 from '../../assets/image/saasModern/icon-5.png';
import FeatureIcon6 from '../../assets/image/saasModern/icon-6.png';

import Screenshot1 from '../../assets/image/saasModern/dash-3.png';
import Screenshot2 from '../../assets/image/saasModern/dash-4.png';

import AuthorOne from 'images/user1.jpg';
import AuthorTwo from 'images/user2.jpg';
import AuthorThree from 'images/user3.jpg';

import { ic_monetization_on } from 'react-icons-kit/md/ic_monetization_on';
import { ic_settings } from 'react-icons-kit/md/ic_settings';
import { pieChart } from 'react-icons-kit/icomoon/pieChart';
import { briefcase } from 'react-icons-kit/fa/briefcase';

export const MENU_ITEMS = [
  {
    label: 'Inicio',
    path: '#banner_section',
    offset: '0',
  },
  {
    label: 'Tu plan',
    path: '#pricing_section',
    offset: '0',
  },
  {
    label: 'Ventajas',
    path: '#feature_section',
    offset: '0',
  },
  {
    label: 'Testimonios',
    path: '#testimonial_section',
    offset: '0',
  },
];

export const PROCESS_ITEMS = [
  {
    image: Process1,
    title: 'Download our app',
    description:
      'Get your blood tests delivered at home collect a sample from the news Get your blood tests delivered with terms.',
  },
  {
    image: Process2,
    title: 'Create a free account',
    description:
      'Get your blood tests delivered at home collect a sample from the news Get your blood tests delivered with terms.',
  },
  {
    image: Process3,
    title: 'Now Start your journey',
    description:
      'Get your blood tests delivered at home collect a sample from the news Get your blood tests delivered with terms.',
  },
];

export const MONTHLY_PRICING_TABLE = [
  {
    name: 'Si inviertes',
    description: 'Retirate en 25 años con',
    price: '$2000',
    priceRetirement: '$3,432,223.00',
    priceLabel: 'Mensuales',
    buttonLabel: 'Ejemplo de proyección',
    url:
      'https://drive.google.com/file/d/1wPJLvUIeZvvxgG4qNLcJ_pWdhB_zjzPK/view?usp=sharing',
    listItems: [],
  },
  {
    name: 'Si inviertes',
    description: 'Retirate en 25 años con',
    price: '$3000',
    priceRetirement: '$5,222,009.00',
    priceLabel: 'Mensuales',
    buttonLabel: 'Ejemplo de proyección',
    url:
      'https://drive.google.com/file/d/1AQQKVH-iq1npysCK9cgeZusQMJauv18J/view?usp=sharing',
    listItems: [],
  },
  {
    name: 'Si inviertes',
    description: 'Retirate en 25 años con',
    price: '$4000',
    priceRetirement: '$7,005,719.00',
    priceLabel: 'Mensuales',
    buttonLabel: 'Ejemplo de proyección',
    url:
      'https://drive.google.com/file/d/19L6427MsWAdkZ-98ZuTTbGIl10tS0cCX/view?usp=sharing',
    listItems: [],
  },
  {
    name: 'Si inviertes',
    description: 'Retirate en 25 años con',
    price: '$5000',
    priceRetirement: '$8,804,620.00',
    priceLabel: 'Mensuales',
    buttonLabel: 'Ejemplo de proyección',
    url:
      'https://drive.google.com/file/d/1TtkGsCubMTOSWmNxe3wI5dHjysE7HHyr/view?usp=sharing',
    listItems: [],
  },
];

export const FAQ_DATA = [
  {
    expend: true,
    title: 'How to contact with Customer Service?',
    description:
      'Our Customer Experience Team is available 7 days a week and we offer 2 ways to get in contact.Email and Chat . We try to reply quickly, so you need not to wait too long for a response!. ',
  },
  {
    title: 'App installation failed, how to update system information?',
    description:
      'Please read the documentation carefully . We also have some online  video tutorials regarding this issue . If the problem remains, Please Open a ticket in the support forum . ',
  },
  {
    title: 'Website reponse taking time, how to improve?',
    description:
      'At first, Please check your internet connection . We also have some online  video tutorials regarding this issue . If the problem remains, Please Open a ticket in the support forum .',
  },
  {
    title: 'New update fixed all bug and issues?',
    description:
      'We are giving the update of this theme continuously . You will receive an email Notification when we push an update. Always try to be updated with us .',
  },
];

import chat from 'common/assets/image/appModern/chat.svg';
import logoWhite from 'images/logoWhite.png';
import github from 'common/assets/image/appModern/github.svg';
import footerLogo from 'images/logotipo-verde.svg';
export const footer = {
  widgets: [
    {
      id: 1,
      title: '',
      description: '',
    },
    {
      id: 2,
      icon: logoWhite,
      title: '',
      description:
        'Sevilla 40, Colonia Juárez 06500, CDMX, México\nAgente de Seguros: NWC Agente de Seguros\nTeléfono: 551500 8812\nCédula Asesor de Inversiones: 90770\n',
    },
    {
      id: 3,
      title: '',
      description: '',
    },
  ],
  logo: footerLogo,
  menu: [
    {
      id: 1,
      text: 'Home',
      link: '#',
    },
    {
      id: 2,
      text: 'Adversite',
      link: '#',
    },
    {
      id: 3,
      text: 'Supports',
      link: '#',
    },
    {
      id: 4,
      text: 'Marketing',
      link: '#',
    },
    {
      id: 5,
      text: 'Contact',
      link: '#',
    },
  ],
};

export const FEATURES = [
  {
    icon: FeatureIcon1,
    title: 'App Development',
    description:
      'Get your proof tests delivered home collect a sample from the news get design.',
  },
  {
    icon: FeatureIcon2,
    title: '10 Times Award',
    description:
      'Get your proof tests delivered home collect a sample from the news get design.',
  },
  {
    icon: FeatureIcon3,
    title: 'Cloud Storage',
    description:
      'Get your proof tests delivered home collect a sample from the news get design.',
  },
  {
    icon: FeatureIcon4,
    title: 'Customization',
    description:
      'Get your proof tests delivered home collect a sample from the news get design.',
  },
  {
    icon: FeatureIcon5,
    title: 'UX Planning',
    description:
      'Get your proof tests delivered home collect a sample from the news get design.',
  },
  {
    icon: FeatureIcon6,
    title: 'Customer Support',
    description:
      'Get your proof tests delivered home collect a sample from the news get design.',
  },
];

export const SCREENSHOTS = [
  {
    icon: ic_monetization_on,
    title: 'Budget Overview',
    image: Screenshot2,
  },
  {
    icon: ic_settings,
    title: 'Create & Adjust',
    image: Screenshot1,
  },
  {
    icon: pieChart,
    title: 'View Reports',
    image: Screenshot2,
  },
  {
    icon: briefcase,
    title: 'Integrations',
    image: Screenshot1,
  },
];

export const TESTIMONIALS = [
  {
    title: 'La mejor decisión',
    review:
      'Contraté un plan con un agente y nunca me dio seguimiento, mi plan tenia un 20% de perdida y no sabia si seguir pagando. Gracias a que contraté el plan mis rendimientos dieron un giro.',
    name: 'Abelardo Martínez',
    designation: '',
    avatar: `${AuthorOne}`,
  },
  {
    title: 'Mi dinero está seguro',
    review:
      'Decidí contratar un plan de retiro, con mi hermana la cual solo duro 2 años vendedora de seguros, no pensé en el riesgo que ponía mi dinero, ahora ya tengo mi portafolio al corriente y bien administrado.',
    name: 'Roberto Macías',
    designation: '',
    avatar: `${AuthorTwo}`,
  },
  {
    title: 'Excelente servicio',
    review:
      'Me vendieron plan en UDIS, y no sabia que yo misma pagaba la UDIS. Tampoco sabia que pagaba un seguro de vida. Yo necesitaba un plan de inversión. Resuelven la dudas rapidamente.',
    name: 'Isabel Nava',
    designation: '',
    avatar: `${AuthorThree}`,
  },
];

export const BENEFITS = [
  { id: 1, title: 'Ahorros deducibles de impuestos' },
  { id: 2, title: 'Inicias con poco dinero' },
  { id: 3, title: 'Cuentas con flexibilidad y liquidez' },
  { id: 4, title: 'Construyes un fondo' },
  { id: 5, title: 'Confianza, deferimiento y exención' },
];

export const BENEFITSBAD = [
  { id: 1, title: 'Solo están capacitados para vender productos' },
  { id: 2, title: 'Solo cuentan con tiempo para buscar nuevos clientes' },
  { id: 3, title: 'No cuentan con cédula de inversión' },
  { id: 4, title: 'Desconocen la administración de portafolios' },
  { id: 5, title: 'No ocupan tecnología' },
  { id: 6, title: 'Solo te acompañan mientras cobran comisión' },
];

export const BENEFITSOK = [
  { id: 1, title: 'Somos asesores de inversiones' },
  { id: 2, title: 'Contamos con un equipo de servicio al cliente' },
  { id: 3, title: 'Tenemos certificaciones por la AMIB' },
  { id: 4, title: 'Ocupamos Modern Portfolio Theory (MTP)' },
  {
    id: 5,
    title:
      'Utilizamos Inteligencia artificial para analizar diario tu porfolio',
  },
  { id: 6, title: 'Te acompañamos durante todo tu plan' },
];
