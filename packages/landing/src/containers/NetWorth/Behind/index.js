import React from 'react';
import { Icon } from 'react-icons-kit';
import Container from 'common/components/UI/ContainerTwo';
import Text from 'common/components/Text';
import Image from 'common/components/Image';
import ourCustomer from 'images/partners.png';
import Behind1 from 'images/behind1.png';
import Behind2 from 'images/behind2.png';
import Behind3 from 'images/behind3.png';
import Behind4 from 'images/behind4.png';
import Behind5 from 'images/behind5.png';
import Fintech from 'images/behind-fintech.png';
import Section, {
  SectionHeading,
  ContentWrapper,
  ImagesWrapper,
  FintechWrapper,
} from './ourCustomer.style';

const Behind = () => {
  return (
    <Section id="our-customer">
      <Container>
        <ContentWrapper>
          <SectionHeading>
            <Text content="Estamos respaldados" fontWeight="bold" />
          </SectionHeading>
          <ImagesWrapper>
            <Image src={Behind1} alt="Forbes" />
            <Image src={Behind2} alt="CB Insights" />
            <Image src={Behind3} alt="500 startups" />
            <Image
              src={Behind4}
              alt="Comisión nacional de seguros y finanzas"
            />
            <Image src={Behind5} alt="Seedstar" />
          </ImagesWrapper>
        </ContentWrapper>
        <FintechWrapper>
          <Image src={Fintech} alt="Fintech" />
        </FintechWrapper>
      </Container>
    </Section>
  );
};

export default Behind;
