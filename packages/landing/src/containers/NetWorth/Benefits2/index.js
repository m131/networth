import React from 'react';
import { Icon } from 'react-icons-kit';
import { chevronRight } from 'react-icons-kit/feather/chevronRight';
import Button from 'common/components/Button';
import Container from 'common/components/UI/Container';
import Text from 'common/components/Text';
import Heading from 'common/components/Heading';
import Image from 'common/components/Image';
import List from 'common/components/List';
import Link from 'common/components/Link';

import SectionWrapper, {
  Section,
  Content,
  Illustration,
  ListGroup,
  GroupedLists,
} from './benefits.style';
import { BENEFITSBAD } from 'common/data/NetWorth';
import { BENEFITSOK } from 'common/data/NetWorth';
import BadTick from 'images/checkx.svg';
import OkTick from 'images/checkv.svg';

const Benefits2 = () => {
  return (
    <SectionWrapper>
      <Container>
        <Section id="feature_section">
          <Content>
            <Heading
              as="h3"
              content="Siempre estarás acompañado"
              textAlign="center"
              color="primary"
            />
            <Heading
              as="h2"
              content="Potencializa tu plan de retiro"
              textAlign="center"
            />
            <Text
              content="netWorth analiza en segundos entre 4399 Fondos de inversión que cotizan en bolsa y que estén disponibles en tu plan para tus objetivos de inversión. Inversiones en fondos de acciones, bonos, bienes raíces, metales preciosos, y energía, de todo el mundo. Nos enfocamos en elegir lo que es mejor para ti."
              textAlign="center"
              lineHeight="0.5"
            />
            <GroupedLists>
              <ListGroup>
                <Heading
                  as="h4"
                  content='Otros "Agentes" (Vendedores)'
                  color="textColor"
                  fontSize="6"
                />
                {BENEFITSBAD.map((item) => (
                  <List
                    className="list-item"
                    key={item.id}
                    text={item.title}
                    icon={
                      <Image
                        src={BadTick}
                        alt="tick icon"
                        height="24"
                        width="24"
                      />
                    }
                  />
                ))}
              </ListGroup>
              <ListGroup>
                <Heading
                  as="h4"
                  content="netWorth"
                  color="textColor"
                  fontSize="6"
                />
                {BENEFITSOK.map((item) => (
                  <List
                    className="list-item"
                    key={item.id}
                    text={item.title}
                    icon={
                      <Image
                        src={OkTick}
                        alt="tick icon"
                        height="24"
                        width="24"
                      />
                    }
                  />
                ))}
              </ListGroup>
            </GroupedLists>
          </Content>
        </Section>
      </Container>
    </SectionWrapper>
  );
};

export default Benefits2;
