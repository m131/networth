import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';
import Box from 'common/components/Box';
import Text from 'common/components/Text';
import Heading from 'common/components/Heading';
import Button from 'common/components/Button';
import Image from 'common/components/Image';
import Container from 'common/components/UI/Container';
import { openModal, closeModal } from '@redq/reuse-modal';

import PartnerSectionWrapper from './partner.style';
import { LinkWrapper } from './partner.style';
import Partner from 'images/podcast.jpg';
import Spotify from 'images/spotify.png';
import Youtube from 'images/youtube.jpg';

/*Engage section*/
const ModalFrame = ({ destUrl }) => (
  <iframe
    title="¡Contrata ahora!"
    width="100%"
    height="100%"
    src={destUrl}
    frameBorder="0"
    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
    allowFullScreen
  />
);

const CloseModalButton = () => (
  <Button
    className="modalCloseBtn"
    variant="fab"
    onClick={() => closeModal()}
    icon={<i className="flaticon-plus-symbol" />}
  />
);

/*Engage Section*/

const PartnerSection = ({
  row,
  col,
  title,
  description,
  button,
  textArea,
  imageArea,
}) => {
  //Engage modal handler
  const handleModal = ({ destUrl }) => {
    openModal({
      config: {
        className: 'video-modal',
        disableDragging: true,
        default: {
          width: '100%',
          height: '100%',
          x: 0,
          y: 0,
        },
      },
      component: ModalFrame,
      componentProps: { destUrl },
      closeComponent: CloseModalButton,
      closeOnClickOutside: true,
    });
  };
  //Engage modal handler

  return (
    <PartnerSectionWrapper>
      <Container>
        <Box {...row}>
          <Box {...col} {...imageArea}>
            <Image src={Partner} alt="Domain Image" />
          </Box>
          <Box {...col} {...textArea}>
            <Text content="podcast" color="primary" />
            <Heading {...title} content="El dinero no viene con instruccines" />
            <LinkWrapper>
              <Image src={Spotify} alt="" />
              <Link href="#!">
                <Text
                  content="10 Mandamientos de Finanzas Personales"
                  fontWeight="bold"
                  mb={0}
                  color="textColor"
                />
              </Link>
            </LinkWrapper>
            <LinkWrapper>
              <Image src={Youtube} alt="" />
              <Link href="#!">
                <Text
                  content="7 pasos para tener Libertad Financiera"
                  fontWeight="bold"
                  mb={0}
                  color="textColor"
                />
              </Link>
            </LinkWrapper>
            <LinkWrapper>
              <Image src={Youtube} alt="" />
              <Link href="#!">
                <Text
                  content="OptiMaxx Plus Allianz Fraude Apestan plan personal de retiro (PPR) inversión en S&P500"
                  fontWeight="bold"
                  mb={0}
                  color="textColor"
                />
              </Link>
            </LinkWrapper>
            <Box mt={15}>
              <Button
                {...button}
                title="Programa una cita"
                onClick={() =>
                  handleModal({ destUrl: 'https://www.networth.mx/solicitud' })
                }
              />
            </Box>
          </Box>
        </Box>
      </Container>
    </PartnerSectionWrapper>
  );
};

PartnerSection.propTypes = {
  row: PropTypes.object,
  col: PropTypes.object,
  title: PropTypes.object,
  description: PropTypes.object,
  button: PropTypes.object,
  textArea: PropTypes.object,
  imageArea: PropTypes.object,
};

PartnerSection.defaultProps = {
  row: {
    flexBox: true,
    flexWrap: 'wrap',
    ml: '-15px',
    mr: '-15px',
    alignItems: 'center',
  },
  imageAreaRow: {
    flexDirection: 'row-reverse',
  },
  col: {
    pr: '15px',
    pl: '15px',
  },
  textArea: {
    width: ['100%', '100%', '55%', '50%', '42%'],
  },
  imageArea: {
    width: ['100%', '100%', '45%', '50%', '58%'],
    mb: ['40px', '40px', '0', '0', '0'],
  },
  title: {
    fontSize: ['26px', '30px', '30px', '48px', '48px'],
    fontWeight: '400',
    color: '#0f2137',
    letterSpacing: '-0.025em',
    mb: '15px',
    lineHeight: '1.25',
  },
  description: {
    fontSize: '16px',
    color: '#343d48cc',
    lineHeight: '1.75',
    mb: '33px',
  },
  button: {
    type: 'button',
    fontSize: '14px',
    fontWeight: '600',
    borderRadius: '4px',
    pl: '22px',
    pr: '22px',
    colors: 'secondaryWithBg',
    minWidth: '150px',
  },
};

export default PartnerSection;
