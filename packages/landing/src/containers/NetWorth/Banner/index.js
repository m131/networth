import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';
import Icon from 'react-icons-kit';
import Fade from 'react-reveal/Fade';
import Box from 'common/components/Box';
import Text from 'common/components/Text';
import Heading from 'common/components/Heading';
import Button from 'common/components/Button';
import Image from 'common/components/Image';
import Container from 'common/components/UI/Container';
import TiltShape from '../TiltShape';
import { openModal, closeModal } from '@redq/reuse-modal';

import { BannerWrapper, DiscountWrapper, DiscountLabel } from './banner.style';
import BannerImage from 'images/banner-image.png';
import { ic_play_circle_filled } from 'react-icons-kit/md/ic_play_circle_filled';
import logoWhite from 'images/logoWhite.png';

/*Engage section*/
const ModalFrame = ({ destUrl }) => (
  <iframe
    title="¡Contrata ahora!"
    width="100%"
    height="100%"
    src={destUrl}
    frameBorder="0"
    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
    allowFullScreen
  />
);

const CloseModalButton = () => (
  <Button
    className="modalCloseBtn"
    variant="fab"
    onClick={() => closeModal()}
    icon={<i className="flaticon-plus-symbol" />}
  />
);

/*Engage Section*/

const BannerSection = ({
  row,
  contentWrapper,
  discountAmount,
  discountText,
  title,
  description,
  imageWrapper,
  buttonWrapper,
  button,
  fillButton,
}) => {
  //Engage modal handler
  const handleModal = ({ destUrl }) => {
    openModal({
      config: {
        className: 'video-modal',
        disableDragging: true,
        default: {
          width: '100%',
          height: '100%',
          x: 0,
          y: 0,
        },
      },
      component: ModalFrame,
      componentProps: { destUrl },
      closeComponent: CloseModalButton,
      closeOnClickOutside: true,
    });
  };
  //Engage modal handler

  return (
    <BannerWrapper id="banner_section">
      <TiltShape />
      <Container>
        <Box {...row}>
          <Box {...contentWrapper}>
            <DiscountWrapper>
              <DiscountLabel>
                <Text {...discountAmount} content="Invierte desde " mt="32px" />
                <Text {...discountText} content="$2000MXN mensuales" />
              </DiscountLabel>
            </DiscountWrapper>
            <Heading {...title} content="Retirate con más de 3 millones." />
            <Text
              {...description}
              fontWeight="bold"
              color="textColor"
              content="¿Quieres invertir en fondos Indexados?"
            />
            <Text
              {...description}
              content="Te enseñamos a invertir en la bolsa de valores para obtener hasta el 12% de rendimiento anual comprando Etf's del S&P500."
            />
            <Box {...buttonWrapper}>
              <a>
                <Button
                  {...button}
                  title="Agenda una asesoría"
                  onClick={() =>
                    handleModal({
                      destUrl: 'https://www.networth.mx/solicitud',
                    })
                  }
                />
              </a>
              <a target="_blank">
                <Button
                  {...fillButton}
                  title="Contrata ahora"
                  iconPosition="left"
                  onClick={() =>
                    handleModal({
                      destUrl: 'https://networthmx.typeform.com/to/XpQgeX',
                    })
                  }
                />
              </a>
              {/*</Link>*/}
            </Box>
          </Box>
          <Box {...imageWrapper}>
            <Fade bottom>
              <Image src={BannerImage} alt="banner image" />
            </Fade>
          </Box>
        </Box>
      </Container>
    </BannerWrapper>
  );
};

BannerSection.propTypes = {
  row: PropTypes.object,
  contentWrapper: PropTypes.object,
  discountAmount: PropTypes.object,
  discountText: PropTypes.object,
  title: PropTypes.object,
  description: PropTypes.object,
  imageWrapper: PropTypes.object,
  buttonWrapper: PropTypes.object,
  button: PropTypes.object,
  fillButton: PropTypes.object,
};

BannerSection.defaultProps = {
  row: {
    flexBox: true,
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentWrapper: {
    width: ['100%', '100%', '80%', '55%', '50%'],
    mb: '40px',
  },
  title: {
    fontSize: ['24px', '32px', '40px', '42px', '46px'],
    fontWeight: '700',
    color: '#fff',
    letterSpacing: '-0.025em',
    mb: ['20px', '25px', '25px', '25px', '25px'],
    lineHeight: '1.2',
    textAlign: 'center',
  },
  description: {
    fontSize: ['15px', '16px', '16px', '16px', '16px'],
    color: '#fff',
    lineHeight: '1.75',
    mb: '0',
    textAlign: 'center',
  },
  discountAmount: {
    fontSize: ['13px', '14px', '14px', '14px', '14px'],
    fontWeight: '700',
    color: '#00865b',
    mb: 0,
    as: 'span',
    mr: '0.4em',
  },
  discountText: {
    fontSize: ['13px', '14px', '14px', '14px', '14px'],
    fontWeight: '700',
    color: '#fff',
    mb: 0,
    as: 'span',
  },
  fillButton: {
    type: 'button',
    fontSize: ['13px', '14px'],
    fontWeight: '600',
    borderRadius: '4px',
    p: ['0px 15px', '8px 22px'],
    colors: 'secondaryWithBg',
    minWidth: ['auto', '150px'],
    height: ['40px', '46px'],
    minHeight: 'auto',
  },
  buttonWrapper: {
    flexBox: true,
    justifyContent: 'center',
    mt: '35px',
  },
  button: {
    type: 'button',
    fontSize: ['13px', '14px'],
    fontWeight: '600',
    borderRadius: '4px',
    p: ['0px 15px', '8px 22px'],
    color: '#fff',
    colors: 'secondary',
    height: ['40px', '46px'],
    minHeight: 'auto',
  },
};

export default BannerSection;
