import React from 'react';
import { Icon } from 'react-icons-kit';

import Container from 'common/components/UI/ContainerTwo';
import Heading from 'common/components/Heading';
import Text from 'common/components/Text';
import Image from 'common/components/Image';
import ourCustomer from 'images/partners.png';
import Section, { SectionHeading, ContentWrapper } from './ourCustomer.style';

const OurCustomer = () => {
  return (
    <Section id="our-customer">
      <Container>
        <ContentWrapper>
          <SectionHeading>
            <Text content="¿Con quién inviertes?" />
            <Heading content="Contamos con contratos de" />
          </SectionHeading>
          <Image
            src={ourCustomer}
            className="illustration"
            alt="Company who also worked us"
          />
        </ContentWrapper>
      </Container>
    </Section>
  );
};

export default OurCustomer;
