import React from 'react';
import { Icon } from 'react-icons-kit';
import { chevronRight } from 'react-icons-kit/feather/chevronRight';
import Button from 'common/components/Button';
import Container from 'common/components/UI/Container';
import Text from 'common/components/Text';
import Heading from 'common/components/Heading';
import Image from 'common/components/Image';
import List from 'common/components/List';
import { openModal, closeModal } from '@redq/reuse-modal';

import SectionWrapper, {
  Section,
  Content,
  Illustration,
  ListGroup,
  CenterButton,
} from './benefits.style';
import { BENEFITS } from 'common/data/NetWorth';
import Tick from 'common/assets/image/agencyModern/tick-circle.png';

/*Engage section*/
const ModalFrame = ({ destUrl }) => (
  <iframe
    title="¡Contrata ahora!"
    width="100%"
    height="100%"
    src={destUrl}
    frameBorder="0"
    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
    allowFullScreen
  />
);

const CloseModalButton = () => (
  <Button
    className="modalCloseBtn"
    variant="fab"
    onClick={() => closeModal()}
    icon={<i className="flaticon-plus-symbol" />}
  />
);

/*Engage Section*/

const Benefits = () => {
  //Engage modal handler
  const handleModal = ({ destUrl }) => {
    openModal({
      config: {
        className: 'video-modal',
        disableDragging: true,
        default: {
          width: '100%',
          height: '100%',
          x: 0,
          y: 0,
        },
      },
      component: ModalFrame,
      componentProps: { destUrl },
      closeComponent: CloseModalButton,
      closeOnClickOutside: true,
    });
  };
  //Engage modal handler

  return (
    <SectionWrapper>
      <Container>
        <Section>
          <Content>
            <Heading
              as="h2"
              content="Networth te ofrece los siguientes beneficios:"
            />
            <ListGroup>
              {BENEFITS.map((item) => (
                <List
                  className="list-item"
                  key={item.id}
                  text={item.title}
                  icon={<Image src={Tick} alt="tick icon" />}
                />
              ))}
            </ListGroup>
          </Content>
        </Section>
      </Container>
      <CenterButton>
        <Button
          title="Programa una cita"
          onClick={() =>
            handleModal({ destUrl: 'https://www.networth.mx/solicitud' })
          }
        />
      </CenterButton>
    </SectionWrapper>
  );
};

export default Benefits;
