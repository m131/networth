import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';
import Icon from 'react-icons-kit';
import Box from 'common/components/Box';
import Text from 'common/components/Text';
import Heading from 'common/components/Heading';
import Button from 'common/components/Button';
import Container from 'common/components/UI/Container';
import GlideCarousel from 'common/components/GlideCarousel';
import GlideSlide from 'common/components/GlideCarousel/glideSlide';
import { openModal, closeModal } from '@redq/reuse-modal';

import { MONTHLY_PRICING_TABLE } from 'common/data/NetWorth';

import PricingTable, {
  CenterButton,
  RetirePlan,
  PricingHead,
  PricingPrice,
  PricingButton,
  PricingList,
  ListItem,
  PricingButtonWrapper,
} from './pricing.style';

import { checkmark } from 'react-icons-kit/icomoon/checkmark';

/*Engage section*/
const ModalFrame = ({ destUrl }) => (
  <iframe
    title="¡Contrata ahora!"
    width="100%"
    height="100%"
    src={destUrl}
    frameBorder="0"
    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
    allowFullScreen
  />
);

const CloseModalButton = () => (
  <Button
    className="modalCloseBtn"
    variant="fab"
    onClick={() => closeModal()}
    icon={<i className="flaticon-plus-symbol" />}
  />
);

/*Engage Section*/

const PricingSection = ({
  sectionWrapper,
  row,
  secTitleWrapper,
  secHeading,
  secText,
  nameStyle,
  descriptionStyle,
  priceStyle,
  priceLabelStyle,
  buttonStyle,
  buttonFillStyle,
  listContentStyle,
  retirementStyle,
}) => {
  const [state, setState] = useState({
    data: MONTHLY_PRICING_TABLE,
    active: true,
  });

  const [loading, setLoading] = useState(false);
  useEffect(() => {
    setTimeout(function () {
      setLoading(true);
    }, 500);
  });

  const data = state.data;
  const activeStatus = state.active;

  const pricingCarouselOptions = {
    type: 'slider',
    perView: 4,
    gap: 15,
    bound: true,
    breakpoints: {
      1199: {
        perView: 3,
        type: 'carousel',
        peek: {
          before: 100,
          after: 100,
        },
      },
      990: {
        type: 'carousel',
        perView: 2,
        peek: {
          before: 160,
          after: 160,
        },
      },
      767: {
        type: 'carousel',
        perView: 1,
        peek: {
          before: 80,
          after: 80,
        },
      },
      575: {
        type: 'carousel',
        perView: 1,
        gap: 15,
        peek: {
          before: 20,
          after: 20,
        },
      },
    },
  };

  //Engage modal handler
  const handleModal = ({ destUrl }) => {
    openModal({
      config: {
        className: 'video-modal',
        disableDragging: true,
        default: {
          width: '100%',
          height: '100%',
          x: 0,
          y: 0,
        },
      },
      component: ModalFrame,
      componentProps: { destUrl },
      closeComponent: CloseModalButton,
      closeOnClickOutside: true,
    });
  };
  //Engage modal handler

  return (
    <RetirePlan>
      <Box {...sectionWrapper} id="pricing_section">
        <Container>
          <Box {...secTitleWrapper}>
            <Text {...secText} content="EMPIEZA HOY" />
            <Heading as="h2" {...secHeading} content="Tu plan de retiro" />
            <Text {...secText} content="En 25 años tendrás" />
          </Box>
          <Box {...row}>
            <GlideCarousel
              carouselSelector="pricing-carousel"
              options={pricingCarouselOptions}
              controls={false}
            >
              <>
                {data.map((pricingTable, index) => (
                  <GlideSlide key={`pricing-table-${index}`}>
                    <PricingTable className="pricing_table">
                      <PricingHead>
                        <Heading content={pricingTable.name} {...nameStyle} />
                      </PricingHead>
                      <PricingPrice>
                        <Text content={pricingTable.price} {...priceStyle} />
                        <Text
                          content={pricingTable.priceLabel}
                          {...priceLabelStyle}
                        />
                        <Text
                          content={pricingTable.description}
                          {...descriptionStyle}
                        />
                        <Text
                          content={pricingTable.priceRetirement}
                          {...retirementStyle}
                        />
                      </PricingPrice>
                      <PricingButton>
                        <Link href={pricingTable.url}>
                          <a>
                            {pricingTable.freePlan ? (
                              <Button
                                title={pricingTable.buttonLabel}
                                {...buttonStyle}
                              />
                            ) : (
                              <Button
                                title={pricingTable.buttonLabel}
                                {...buttonFillStyle}
                              />
                            )}
                          </a>
                        </Link>
                      </PricingButton>
                      <PricingList>
                        {pricingTable.listItems.map((item, index) => (
                          <ListItem key={`pricing-table-list-${index}`}>
                            <Icon
                              icon={checkmark}
                              className="price_list_icon"
                              size={13}
                            />
                            <Text
                              content={item.content}
                              {...listContentStyle}
                            />
                          </ListItem>
                        ))}
                      </PricingList>
                    </PricingTable>
                  </GlideSlide>
                ))}
              </>
            </GlideCarousel>
          </Box>
        </Container>
      </Box>
      <Heading
        as="h3"
        textAlign="center"
        fontSize="8"
        content="¿Cómo se obtiene esa cantidad?"
      />
      <Text
        content="Gracias al interés compuesto"
        textAlign="center"
        color="#5c5c5c"
        fontWeight="bold"
      />
      <CenterButton>
        <Button
          title="Solicita más información"
          onClick={() =>
            handleModal({ destUrl: 'https://www.networth.mx/solicitud' })
          }
        />
      </CenterButton>
    </RetirePlan>
  );
};

PricingSection.propTypes = {
  sectionWrapper: PropTypes.object,
  row: PropTypes.object,
  secTitleWrapper: PropTypes.object,
  secHeading: PropTypes.object,
  secText: PropTypes.object,
  nameStyle: PropTypes.object,
  descriptionStyle: PropTypes.object,
  priceStyle: PropTypes.object,
  priceLabelStyle: PropTypes.object,
  retirementStyle: PropTypes.object,
  listContentStyle: PropTypes.object,
};

PricingSection.defaultProps = {
  sectionWrapper: {
    as: 'section',
    pt: ['100px', '100px', '100px', '140px', '160px'],
    pb: ['60px', '80px', '80px', '100px'],
  },
  row: {
    flexBox: true,
    flexWrap: 'wrap',
    ml: '-15px',
    mr: '-15px',
    alignItems: 'center',
  },
  secTitleWrapper: {
    mb: ['50px', '75px'],
  },
  secText: {
    as: 'span',
    display: 'block',
    textAlign: 'center',
    fontSize: '14px',
    letterSpacing: '0.15em',
    fontWeight: '700',
    color: '#2aa275',
    mb: '5px',
  },
  secHeading: {
    textAlign: 'center',
    fontSize: ['20px', '24px'],
    fontWeight: '500',
    color: '#0f2137',
    letterSpacing: '-0.025em',
    mb: '0',
    lineHeight: '1.67',
  },
  col: {
    width: [1, 1 / 2, 1 / 2, 1 / 3],
    pr: '15px',
    pl: '15px',
  },
  nameStyle: {
    fontSize: '1rem',
    fontWeight: '500',
    color: 'headingColor',
    letterSpacing: '-0.025em',
    textAlign: 'center',
    mb: '12px',
  },
  descriptionStyle: {
    fontSize: ['15px', '16px', '16px', '16px', '16px'],
    color: 'textColor',
    lineHeight: '1.75',
    textAlign: 'center',
    mb: '0',
  },
  retirementStyle: {
    fontSize: ['1.5rem'],
    fontWeight: 'bold',
    color: 'textColor',
    textAlign: 'center',
    mt: '1',
  },
  priceStyle: {
    as: 'span',
    display: 'block',
    fontSize: ['36px', '36px', '40px', '40px', '40px'],
    color: 'headingColor',
    textAlign: 'center',
    mb: '5px',
    letterSpacing: '-0.025em',
  },
  priceLabelStyle: {
    fontSize: ['13px', '14px', '14px', '14px', '14px'],
    color: 'textColor',
    lineHeight: '1.75',
    textAlign: 'center',
    mb: '0',
  },
  buttonStyle: {
    type: 'button',
    fontSize: '14px',
    fontWeight: '600',
    borderRadius: '4px',
    pl: '10px',
    pr: '10px',
    bg: '#fff',
    color: '#2aa275',
    colors: 'primaryWithBg',
    width: '222px',
    maxWidth: '100%',
  },
  buttonFillStyle: {
    type: 'button',
    fontSize: '14px',
    fontWeight: '600',
    color: 'white',
    borderRadius: '4px',
    pl: '10px',
    pr: '10px',
    colors: 'primaryWithBg',
    width: '200px',
    maxWidth: '100%',
  },
  listContentStyle: {
    fontSize: ['15px', '16px', '16px', '16px', '16px'],
    color: 'textColor',
    mb: '0',
  },
};

export default PricingSection;
