import React, { Fragment } from 'react';
import Head from 'next/head';
import { ThemeProvider } from 'styled-components';
import { NetWorthTheme } from 'common/theme/netWorth';
import { ResetCSS } from 'common/assets/css/style';
import {
  GlobalStyle,
  ContentWrapper,
} from 'containers/NetWorth/netWorth.style';

import Navbar from 'containers/NetWorth/Navbar';
import BannerSection from 'containers/NetWorth/Banner';
import PricingSection from 'containers/NetWorth/Pricing';
import VideoSection from 'containers/NetWorth/VideoSection';
import VideoSection2 from 'containers/NetWorth/VideoSection2';
import Benefits from 'containers/NetWorth/Benefits';
import Benefits2 from 'containers/NetWorth/Benefits2';
import Podcast from 'containers/NetWorth/Podcast';
import Partners from 'containers/NetWorth/Partners';
import TestimonialSection from 'containers/NetWorth/Testimonial';
import Behind from 'containers/NetWorth/Behind';
import Footer from 'containers/NetWorth/Footer';

const NetWorth = () => {
  return (
    <ThemeProvider theme={NetWorthTheme}>
      <Fragment>
        <Head>
          <title>netWorth | Inteligencia Artificial</title>
          <meta name="Description" content="React next landing page" />
          <meta name="theme-color" content="#ec5555" />
          {/* Load google fonts */}
          <link
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700,900|Open+Sans:400,400i,600,700"
            rel="stylesheet"
          />
        </Head>

        <ResetCSS />
        <GlobalStyle />

        <ContentWrapper>
          <BannerSection />
          <PricingSection />
          <VideoSection />
          <Benefits />
          <Benefits2 />
          <VideoSection2 />
          <Podcast />
          <Partners />
          <TestimonialSection />
          <Behind />
          <Footer />
        </ContentWrapper>
      </Fragment>
    </ThemeProvider>
  );
};
export default NetWorth;
